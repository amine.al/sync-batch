## poc to test db to db sync via spring batch

- create a mysql server :
`docker run --name sage-mysql -e -p 3306:3306 MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:latest`

- create a database : 
    - get in server : `docker exec -it sage-mysql bash`
    - connect to mysql : `mysql -u root -p`

````
CREATE DATABASE sage_db;
USE DATABASE sage_db;
DROP TABLE IF EXISTS F_ARTICLE
CREATE TABLE F_ARTICLE ( AR_Ref varchar(255), AR_Design varchar(255), FA_CodeFamille varchar(255), AR_Garantie varchar(255) );
````
