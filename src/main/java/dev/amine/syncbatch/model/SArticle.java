package dev.amine.syncbatch.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SArticle {

	private String arRef;

	private String arDesign;

	private String faCodeFamille;

	private String arGarantie;

}
