package dev.amine.syncbatch.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Article {
	
	private String codeArticle;

	private String codeFournisseur;

	private String codeSousFamille;

	private String fabricant;

	private String marque;

	private String gamme;

	private String referenceCommerciale;

	private String referenceArticle;

	private String referenceInformatique;

	private String gtin13;

	private String gtin14;

	private String libelle30;

	private String libelle80;

	private String libelle240;

	private String dureeGarantie;

	private String statutCommercialisation;

	private String dateStatutCommercialisation;

}
