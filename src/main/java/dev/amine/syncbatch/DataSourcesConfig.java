package dev.amine.syncbatch;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class DataSourcesConfig {

    @Bean
    @ConfigurationProperties("spring.datasource.centraldb")
    public DataSourceProperties centralDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.sagedb")
    public DataSourceProperties sageDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    public DataSource centralDataSource() {
        return centralDataSourceProperties()
        .initializeDataSourceBuilder()
        .build();
    }

    @Bean
    public DataSource sageDataSource() {
        return sageDataSourceProperties()
        .initializeDataSourceBuilder()
        .build();
    }

    @Bean
    public JdbcTemplate centralJdbcTemplate(@Qualifier("centralDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public JdbcTemplate sageJdbcTemplate(@Qualifier("sageDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
