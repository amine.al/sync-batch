package dev.amine.syncbatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.item.ItemProcessor;

import dev.amine.syncbatch.model.Article;
import dev.amine.syncbatch.model.SArticle;

public class ArticleItemProcessor implements ItemProcessor<Article, SArticle> {

	private static final Logger log = LoggerFactory.getLogger(ArticleItemProcessor.class);

	@Override
	public SArticle process(final Article article) throws Exception {


		final SArticle sageArticle = SArticle
				.builder()
				.arDesign(article.getLibelle80())
				.arGarantie(article.getDureeGarantie())
				.arRef(article.getCodeArticle())
				.build();

		log.info("Converting (" + article + ") into (" + sageArticle + ")");

		return sageArticle;
	}

}
