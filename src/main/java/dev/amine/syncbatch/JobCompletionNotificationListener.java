package dev.amine.syncbatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import dev.amine.syncbatch.model.SArticle;

@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

	private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public JobCompletionNotificationListener(@Qualifier("sageJdbcTemplate") JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("!JOB FINISHED! Time to verify the results");

			jdbcTemplate.query("SELECT AR_Ref, AR_Design, FA_CodeFamille, AR_Garantie FROM F_ARTICLE",
				(rs, row) ->  SArticle.builder()
				.arRef(			rs.getString(1))
				.arDesign(		rs.getString(2))
				.faCodeFamille(	rs.getString(3))
				.arGarantie(	rs.getString(4))
				.build()
			).forEach(article -> log.info("Found <" + article + "> in the database."));
		}
	}
}
