package dev.amine.syncbatch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcPagingItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import dev.amine.syncbatch.model.Article;
import dev.amine.syncbatch.model.SArticle;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;
    
    @Bean
	public JdbcPagingItemReader<Article> reader(@Qualifier("centralDataSource") DataSource dataSource) {
		return new JdbcPagingItemReaderBuilder<Article>()
			.name("articlesReader")
			.selectClause("SELECT code_article, libelle_80, duree_garantie")
			.fromClause("articles")
			.rowMapper(new RowMapper<Article>() {

				@Override
				@Nullable
				public Article mapRow(ResultSet rs, int rowNum) throws SQLException {
					return Article
						.builder()
						.codeArticle(rs.getString("code_article"))
						.libelle80(rs.getString("libelle_80"))
						.dureeGarantie(rs.getString("duree_garantie"))
						.build();		
				}
				
			})
			.sortKeys(Map.of("code_article", Order.ASCENDING))
			.dataSource(dataSource)
			.build();
	}

	@Bean
	public ArticleItemProcessor processor() {
		return new ArticleItemProcessor();
	}

	@Bean
	public JdbcBatchItemWriter<SArticle> writer(@Qualifier("sageDataSource") DataSource dataSource) {
		return new JdbcBatchItemWriterBuilder<SArticle>()
			.itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
			// INSERT INTO F_ARTICLE(AR_Ref, AR_Design, FA_CodeFamille, AR_Garantie) 
			// VALUES ('004000', 'TEST Pile de rechange type SR44 pour PAC','ZESCZZUNZ0',12);
			.sql("""
				INSERT INTO F_ARTICLE (AR_Ref, AR_Design, FA_CodeFamille, AR_Garantie) 
				VALUES (:arRef, :arDesign, :faCodeFamille, :arGarantie)
				"""
			)
			.dataSource(dataSource)
			.build();
	}

	@Bean
	public Job importUserJob(JobCompletionNotificationListener listener, Step step) {
		return jobBuilderFactory.get("importUserJob")
			.incrementer(new RunIdIncrementer())
			.listener(listener)
			.flow(step)
			.end()
			.build();
	}

	@Bean
	public Step step(JdbcPagingItemReader<Article> reader, JdbcBatchItemWriter<SArticle> writer) {
		return stepBuilderFactory.get("step")
			.<Article, SArticle> chunk(10)
			.reader(reader)
			.processor(processor())
			.writer(writer)
			.build();
	}
}
